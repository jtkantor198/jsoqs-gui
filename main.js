const {app, BrowserWindow, ipcMain} = require('electron')
let fs = require("fs");
let JsoqDb = require("jsoqs-db")
let DGraphAdapter = require("jsoq-dgraph")
// Importing this adds a right-click menu with 'Inspect Element' option

  // Keep a global reference of the window object, if you don't, the window will
  // be closed automatically when the JavaScript object is garbage collected.
let win, db;
ipcMain.on('schema', async (event, arg) => {
  arg=JSON.parse(arg);
  console.log(event, arg) // prints "ping"
  db = await (new JsoqDb({
      adapter: new DGraphAdapter({
          host: 'localhost',
          port: 9080
      }),
      structure: arg.structure,
      collections: arg.collections
  }))
  event.sender.send('db-ready', true)
})

ipcMain.on('jsoq', async (event, arg) => {
  console.log(arg);
  event.sender.send('results',await db.query(arg));
})

function createWindow () {
    // Create the browser window.
    win = new BrowserWindow({width: 800, height: 600})
    win.fs = fs;
    win.setMenu(null);
    // and load the index.html of the app.
    win.loadFile('index.html')
  
    // Open the DevTools.
    win.webContents.openDevTools()
  
    // Emitted when the window is closed.
    win.on('closed', () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      win = null
    })
  }
  
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
// On macOS it is common for applications and their menu bar
// to stay active until the user quits explicitly with Cmd + Q
if (process.platform !== 'darwin') {
    app.quit()
}
})

app.on('activate', () => {
// On macOS it's common to re-create a window in the app when the
// dock icon is clicked and there are no other windows open.
if (win === null) {
    createWindow()
}
})
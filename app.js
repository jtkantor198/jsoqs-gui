//import Vue from 'vue/dist/vue'
//import VueRouter from 'vue-router/dist/vue-router'
//import Vuetify from 'vuetify/dist/vuetify'
//import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
const {ipcRenderer,remote} = require('electron')
const {MenuItem,Menu} = remote;

let rightClickPosition = null

const menu = new Menu()
const menuItem = new MenuItem({
  label: 'Inspect Element',
  click: () => {
    remote.getCurrentWindow().inspectElement(rightClickPosition.x, rightClickPosition.y)
  }
})
menu.append(menuItem)

window.addEventListener('contextmenu', (e) => {
  e.preventDefault()
  rightClickPosition = {x: e.x, y: e.y}
  menu.popup(remote.getCurrentWindow())
}, false)

import 'material-design-icons-iconfont/dist/material-design-icons.css'
//import './app.css'
/*
import Feed from './pages/feed/feed.js'
*/
import Topbar from './components/topbar/topbar.js';
import Sidebar from './components/sidebar/sidebar.js';
import FlexTable from './components/flex-table/flex-table.js'

import fs from 'fs';
//import TreeView from "vue-json-tree-view"

console.log(Topbar, Sidebar, fs);

Vue.use(Vuetify)
//Vue.use(VueRouter);


new Vue({
    template: `
    <v-app>
    <side-bar :show="show" :title="sideBarTitle" :items="sidebarItems"></side-bar>
    <!--<top-bar v-on:toggleSidebar="show=!show"></top-bar>-->
    <v-content>
        <v-container fluid>
        <div v-show="starting">
        <v-textarea
        solo
        outline
        label="Structure"
        :value="structure"
        ></v-textarea>
        <v-textarea
        solo
        outline
        label="Collections"
        :value="collections"
        ></v-textarea>
        <v-btn @click="start()">Start</v-btn>
        </div>
        <div v-show="!starting">
            <v-text-field label="JSOQ" :value="jsoq" @change="updateJsoq"></v-text-field><v-btn @click="runQuery()">Go</v-btn>
            <flex-table :table="table" :mode="mode" :jsonViewOptions="{rootObjectKey: currentCollection, maxDepth: 1}"></flex-table>
        </div>
        </v-container>
    </v-content>
    </v-app>`,
    el: '#app',
    /*router: new VueRouter({
        routes: [{path: '/', name: 'feed', component: Feed}]
    }),*/
    components: {
        "top-bar": Topbar,
        "side-bar": Sidebar,
        "flex-table": FlexTable
        //"flex-table": FlexTable
    },
    data: ()=>{
        return {
            headers: [{ text: 'Country', value: 'name' }],
            results: [{name: "France"}],
            sidebar: {},
            show: undefined,
            starting: true,
            currentCollection: "city",
            structure: `
                download
                }->article : article : downloads     
                    }->journal : journal : articles
                    }-{author : authors : articles
                    }-{article : references : references
                }->city : city : downloads
                    }->country : country : cities
            `,
            collections: {
                article: {
                    doi: {type: "string", uniqueKey: true},
                    title: {type: "string"}
                }, 
                author: {
                    name: {type: "string", uniqueKey: true},
                }, 
                city: {
                    name: {type: "string", uniqueKey: true},
                }, 
                country: {
                        name: {type: "string", uniqueKey: true},
                }, 
                download: {
                    time: {type: "datetime"},
                    latitude: {type: "number"},
                    longitude: {type: "number"}
                }, 
                journal: {
                    name: {type: "string", uniqueKey: true},
                }
            },
            jsoq: "",
            table: {
                columns: [
                    {label: "Results"},
                ],
                rows: []
            },
            mode: "json",
            jsonViewOptions: {
                rootObjectKey: "city",
                maxDepth: 1
            },
            sideBarTitle: "Collections"
        }
    },
    created(){
        console.log(this)
    },
    methods: {
        start: async function(){
            /*this.db = await new JsoqDb({
                adapter: new DGraphAdapter({
                    host: 'localhost',
                    port: 9080
                }),
                structure: structure,
                collections: collections
            })*/
            console.log(this);
            //console.log(ipcRenderer.sendSync('synchronous-message', )) // prints "pong"
            
            ipcRenderer.on('db-ready', (event, arg) => {
                this.starting = false;

            })
            ipcRenderer.send('schema', JSON.stringify({structure: this.structure, collections: this.collections}))
            console.log("sent")
        },
        runQuery: async function(){
            console.log(this.jsoq);
            ipcRenderer.send('jsoq',this.jsoq);
            ipcRenderer.on('results', (event, arg) => {
                console.log(arg);
                this.table.rows.splice(0, this.table.rows.length);
                this.table.rows.push(...arg);
            })
        },
        updateJsoq(str){
            console.log(str);
            this.jsoq =  str;
        }
    },
    watch: {
        '$refs.sidebar': function(value){
            this.sidebar = value
        }
    },
    computed: {
        sidebarItems: function(){
            if (this.collections instanceof Object){
                return Object.keys(this.collections).map((name)=>{return {
                    title: name, 
                    icon: "",
                    onClick: ()=>{
                        this.jsoq=`${name}<limit:10>`
                        this.currentCollection = name;
                        this.runQuery();
                    }
                }})
            }
            else{
                return [];
            }
        }
    }
})
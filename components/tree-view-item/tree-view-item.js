  import _ from 'lodash'
  import TreeViewItemValue from '../tree-view-item-value/tree-view-item-value.js'
  import './tree-view-item.scss';

  export default {
    template: require("./tree-view-item.html"),
    components: {
      "tree-view-item-value": TreeViewItemValue
    },
  	name: "tree-view-item",
    props: ["data", "max-depth", "current-depth", "modifiable"],
    data: function(){
    	return {
      	open: this.currentDepth < this.maxDepth
      }
    },
    methods: {
      isOpen: function(){
      	return this.open;
      },
      toggleOpen:function(){
      	this.open = !this.open;
      },
    	isObject: function(value){
      	return value.type === 'object';
      },
    	isArray: function(value){
      	return value.type === 'array';
      },
      isValue: function(value){
      	return value.type === 'value';
      },
      getKey: function(value){
      	if (_.isInteger(value.key)) {
        	return value.key+":";
        } else {
  	      return "\""+ value.key + "\":";
        }
      },
      isRootObject: function(value = this.data){
      	return value.isRoot;
      },
      onChangeData: function(path, value) {
        path = _.concat(this.data.key, path)
        this.$emit('change-data', path, value)
      }
    }
  }
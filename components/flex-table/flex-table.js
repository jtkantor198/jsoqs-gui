import FlexItem from '../flex-item/flex-item.js';
import TreeView from "../tree-view/tree-view.js";

export default {
    template: require("./flex-table.html"),
    props: ["table","mode","jsonViewOptions"],
    computed: {
        headers(){
            return this.table ? this.table.columns.map((col)=>{return{
                text: col.label,
                value: col.path
            }}) : [];
        },
        jsnViewOptions: function(){
            return {...(this.jsonViewOptions===undefined ? {} : this.jsonViewOptions)};
        }
    },
    data: ()=>{return {
        thing: '{"country":{"name":"Greece"},"name":"Athina"}'
    }},
    components: {
        'flex-item': FlexItem,
        'tree-view': TreeView
    },
    methods: {
        json: function(item){
            return JSON.stringify(item)
        }
    }
}
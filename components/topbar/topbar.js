export default {
    template: require("./topbar.html"),
    data: ()=>{
        return {
            expand: false
        }
    },
    props: ['sidebar'],
    methods: {
        toggleSidebar: function(){
            //console.log(this.sidebar);
            this.$emit('toggleSidebar')
        }
    }
};
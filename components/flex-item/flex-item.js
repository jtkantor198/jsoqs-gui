

export default {
    template: require("./flex-item.html"),
    props: ["object","path"],
    computed: {
        value: function(){
            let ref = this.object;
            console.log(this);
            if (this.path===undefined){
                return "";
            }
            let keys = this.path.split(".");
            for (let k of keys){
                ref = ref[k];
            }
            console.log(ref);
            return ref
        }
    }
}
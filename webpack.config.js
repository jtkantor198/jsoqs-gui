module.exports = {
    mode: "development",
    entry: {
      main: './app.js'
    },
    module: {
        rules: [
            {test: /\.scss$/, use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS
            ] },
            {test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
            {test: /\.html$/, use: [{loader: 'html-loader', options: {minimize: true}}]},
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    },
    output: {
        filename: 'bundle.js',
        path: __dirname+'/build'
    },
    watch: true,
    target: 'electron-main'
};
  